require 'adioh'
require 'json'
require 'stringio'

describe ADIOH do
  before :all do
    # Mock all the things
    Fog.mock!
    @a = ADIOH::OpenStackInventory.new
  end

  after :all do
    Fog.unmock!
  end

  describe '#emit_inventory' do
    context 'when ADIOH_ACTIVE_ONLY environment variable is set' do
      before :all do
        ENV['ADIOH_ACTIVE_ONLY'] = 'true'
      end

      it 'does not return inactive hypervisors' do
        hypervisors = []
        json = StringIO.new
        $stdout = json
        @a.emit_inventory(true)
        data = JSON.parse($stdout.string)
        json.close
        data.each do |key, val|
          next if key == '_meta'
          val.each do |host|
            hypervisors.push host
          end
        end
        expect(hypervisors.size).to eq(3)
      end

      after :all do
        ENV['ADIOH_ACTIVE_ONLY'] = nil
      end
    end
  end

  describe '#host_aggregates' do
    it 'returns a list of aggregates from Nova' do
      groups = @a.host_aggregates
      expect(groups.size).to eq(2)
    end

    context 'when a host aggregate is empty' do
      it 'does not return the host aggregate' do
        groups = @a.host_aggregates
        expect(groups.size).to eq(2)
      end
    end
  end

  describe '#hypervisors' do
    it 'returns a list of hosts from Nova' do
      hypervisors = @a.hypervisors
      expect(hypervisors.size).to eq(5)
    end

    context 'when ADIOH_ACTIVE_ONLY environment variable is set' do
      before :all do
        ENV['ADIOH_ACTIVE_ONLY'] = 'true'
      end

      it 'does not return hypervisors whose status is "disabled" nor whose state
      is "down"' do
        hypervisors = @a.hypervisors
        expect(hypervisors.size).to eq(3)
      end

      after :all do
        ENV['ADIOH_ACTIVE_ONLY'] = nil
      end
    end
  end # describe
end # adioh
