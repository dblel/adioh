Ansible Dynamic Inventory of OpenStack Hypervisors (ADIOH)
==========================================================

## About

When working on the infrastructure side of an OpenStack cloud, it's often useful to have a tool such as Ansible at one's disposal for mass interrogation of the infrastructure. Ansible provides the ability (via inventory files) to perform ad-hoc commands across an entire OpenStack cloud or a subset (think host aggregates and AZs) thereof. Maintaining inventory files isn't much fun. Fortunately, Ansible can leverage [dynamic inventory scripts](http://docs.ansible.com/ansible/intro_dynamic_inventory.html) which take some (all?) of the pain out of managing your inventory. ADIOH (sounds like 'a-dee-oh') is currently focused around working with compute hosts which are organized into several host aggregates and availability zones. Perhaps one day it will be expanded to include control plane infrastructure.

## Environment Variables 

There are several required environment variables which need to be set in order for the inventory script to function:

| Name                         | Example                      | Default                      |
| --------                     | --------                     | -------                      |
| OS_USERNAME                  | admin                        | _none_                       |
| OS_PASSWORD                  | s3cr3t                       | _none_                       |
| OS_PROJECT_NAME              | admin                        | _none_                       |
| OS_AUTH_URL                  | http://keystone:5000/v3      | _none_                       |
| ADIOH_CACHE_FILE (optional)  | /home/alice/adioh_cache.json | '/tmp/.adioh_inventory.json' |
| ADIOH_SSH_USER (optional)    | alice                        | 'root'                       |
| ADIOH_ACTIVE_ONLY (optional) | true                         | _none_                       |

Most variables are self-explanatory. All of the 'OS_' prefixed variables are the OpenStack Keystone authentication bits.

Note that, ```ADIOH_CACHE_FILE``` specifies a location on the filesystem (writeable by your user of course) where the inventory data will be cached. This caching has major efficiencies because it avoids un-necessary API calls to get information that likely hasn't changed a whole lot. ```ADIOH_ACTIVE_ONLY``` can be set to 'true' (or anything that evaluates to not nil in Ruby) to force ADIOH to only return hypervisors in the inventory whose state is 'up' and whose status is 'enabled'.

## Prerequisites
- Ruby 2.0.0 or above
- Bundler

## Installation

Execute:

```sh
$ bundle
```

That's it!

## Usage

### Standalone

##### Populate the inventory
``` bash
$ ./inventory.rb --refresh
```
``` json
{
  "BigData_All_SSD": [
    "hadoop001_r730xd",
    "hadoop002_r730xd",
    "hadoop003_r730xd"
  ]
  "_meta": {
    "hostvars": {
      "hadoop001_r730xd": {
        "ansible_host": "192.168.1.101",
        "ansible_user": "bofh"
      },
      "hadoop002_r730xd": {
        "ansible_host": "192.168.1.102",
        "ansible_user": "bofh"
      },
      "hadoop003_r730xd": {
        "ansible_host": "192.168.1.103",
        "ansible_user": "bofh"
      }
    }
  }
}
```

##### List hosts in the cache
``` bash
$ ./inventory.rb --list
```
``` json
{
  "BigData_All_SSD": [
    "hadoop001_r730xd",
    "hadoop002_r730xd",
    "hadoop003_r730xd"
  ]
  "_meta": {
    "hostvars": {
      "hadoop001_r730xd": {
        "ansible_host": "192.168.1.101",
        "ansible_user": "bofh"
      },
      "hadoop002_r730xd": {
        "ansible_host": "192.168.1.102",
        "ansible_user": "bofh"
      },
      "hadoop003_r730xd": {
        "ansible_host": "192.168.1.103",
        "ansible_user": "bofh"
      }
    }
  }
}

```

### Ansible integration

##### Mocking
``` bash
$ ./inventory.rb --mock > /dev/null
$ export ADIOH_CACHE_FILE=/tmp/.adioh_inventory_mock.json
$ ansible all -i inventory.rb --list-hosts
  hosts (5):
    compute004
    compute005
    compute001
    compute002
    compute003
```

##### Operate on all hosts
``` bash
$ ansible all -i inventory.rb -m shell -a 'uptime'
<LOTS OF OUTPUT HERE PROBABLY>
```

##### Operate on all hosts in 'Performance_Tier1' host aggregate
``` bash
$ ansible Performance_Tier1 -i inventory.rb -m shell -a 'uptime'
compute001 | SUCCESS | rc=0 >>
 14:36:41 up 35 days, 23:18,  1 user,  load average: 0.14, 0.07, 0.06
```

##### Operate on a group of hosts matching a regex
``` bash
$ ansible "~.*compute00[1-3]" -i inventory.rb -m ping
medium-compute002 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
large-compute003 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
small-compute001 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
```
