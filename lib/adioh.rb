require 'fog/openstack'
require 'json'

# Ansible Dynamic Inventory of OpenStack Hypervisors
module ADIOH
  module Real
    # Fog::Compute::OpenStack::Real#list_hosts does not return hypervisor `id`
    # which is needed for getting detailed information about the hypervisors
    # (e.g. `host_ip`, etc.).
    # Therefore, we need to add a method which does return `id` and use
    # it so that we can pass `id` to another custom method:
    # `get_hypervisor_details`
    def list_hypervisors(options = {})
      request(
        expects: [200, 203],
        method:  'GET',
        path:    'os-hypervisors.json',
        query:   options
      )
    end

    # Complement to #list_hypervisors
    def get_hypervisor_details(host)
      request(
        expects: [200, 203],
        method:  'GET',
        path:    "os-hypervisors/#{host}.json"
      )
    end
  end

  module Mock
    def list_aggregates(options = {})
      response = Excon::Response.new
      response.status = 200
      response.body = {
        'aggregates' => [
          {
            'id'                => 1,
            'name'              => 'Performance_Tier1',
            'hosts'             => [
              'compute001',
              'compute002',
              'compute003'
            ]
          },
          {
            'id'                => 2,
            'name'              => 'Performance_Tier2',
            'hosts'             => [
              'compute004',
              'compute005'
            ]
          },
          {
            'id'                => 3,
            'name'              => 'Lowest_Tier',
            'hosts'             => [
            ]
          }
        ]
      }
      response
    end

    def list_hypervisors(options = {})
      response = Excon::Response.new
      response.status = 200
      response.body = {
        'hypervisors' => [
          {
            'status'              => 'enabled',
            'state'               => 'up',
            'id'                  => 1,
            'hypervisor_hostname' => 'compute001.somecloud.example'
          },
          {
            'status'              => 'enabled',
            'state'               => 'up',
            'id'                  => 2,
            'hypervisor_hostname' => 'compute002.somecloud.example'
          },
          {
            'status'              => 'enabled',
            'state'               => 'up',
            'id'                  => 3,
            'hypervisor_hostname' => 'compute003.somecloud.example'
          },
          {
            'status'              => 'disabled',
            'state'               => 'up',
            'id'                  => 4,
            'hypervisor_hostname' => 'compute004.somecloud.example'
          },
          {
            'status'              => 'disabled',
            'state'               => 'down',
            'id'                  => 5,
            'hypervisor_hostname' => 'compute005.somecloud.example'
          }
        ]
      }
      response
    end

    def get_hypervisor_details(host)
      response = Excon::Response.new
      response.status = 200
      case host
      when 1
        response.body = {
          'hypervisor' => {
            'status'  => 'enabled',
            'state'   => 'up',
            'id'      => 1,
            'service' => {
              'host' => 'compute001'
            },
            'host_ip' => '10.33.66.1'
          }
        }
      when 2
        response.body = {
          'hypervisor' => {
            'status'  => 'enabled',
            'state'   => 'up',
            'id'      => 2,
            'service' => {
              'host' => 'compute002'
            },
            'host_ip' => '10.33.66.2'
          }
        }
      when 3
        response.body = {
          'hypervisor' => {
            'status'  => 'enabled',
            'state'   => 'up',
            'id'      => 3,
            'service' => {
              'host' => 'compute003'
            },
            'host_ip' => '10.33.66.3'
          }
        }
      when 4
        response.body = {
          'hypervisor' => {
            'status'  => 'disabled',
            'state'   => 'up',
            'id'      => 4,
            'service' => {
              'host' => 'compute004'
            },
            'host_ip' => '10.33.66.4'
          }
        }
      else
        response.body = {
          'hypervisor' => {
            'status'  => 'disabled',
            'state'   => 'down',
            'id'      => 5,
            'service' => {
              'host' => 'compute005'
            },
            'host_ip' => '10.33.66.5'
          }
        }
      end
      response
    end # get_hypervisor_details
  end # mock
end # module

module Fog
  module Compute
    class OpenStack
      class Real
        include ADIOH::Real
      end
      class Mock
        # Need to prepend instead of include so that some mocks are overridden.
        # Must be using Ruby >= 2.0 for prepend to work.
        prepend ADIOH::Mock
      end # mock
    end # openstack
  end # compute
end # fog

module ADIOH
  class OpenStackInventory
    def initialize(args = {})
      # Set cache file location to default is not set in ENV
      @cache_file     = if args[:adioh_cache_file].nil?
                          '/tmp/.adioh_inventory.json'
                        else
                          args[:adioh_cache_file]
                        end

      @adioh_ssh_user = if args[:adioh_ssh_user].nil?
                          'root'
                        else
                          args[:adioh_ssh_user]
                        end
    end

    def host_aggregates
      authenticate!
      response = @compute.list_aggregates[:body]
      @aggregates = []
      response['aggregates'].each do |agg|
        # Do not display empty aggregates
        @aggregates.push agg unless agg['hosts'].empty?
      end
      @aggregates
    end

    def hypervisors
      authenticate!
      response = @compute.list_hypervisors[:body]
      @hypervisors = []
      response['hypervisors'].each do |h|
        # Don't return 'disabled' or 'down' hypervisors
        if ENV['ADIOH_ACTIVE_ONLY'].nil?
          @hypervisors.push h
        elsif h['state'] == 'down'
          next
        elsif h['status'] == 'disabled'
          next
        else
          @hypervisors.push h
        end
      end
      @hypervisors
    end

    # rubocop:disable Metrics/AbcSize
    def emit_inventory(refresh = false)
      # If we want fresh data, we've got some work to do
      if refresh
        hypervisors
        compile_meta
        host_aggregates
        data = {}
        @aggregates.each do |agg|
          data[agg['name']] = []
          agg['hosts'].each do |host|
            data[agg['name']].push host if @active_hypervisors.include? host
          end
          data.delete(agg['name']) if data[agg['name']].empty?
        end

        data[:_meta] = @meta
        # Write inventory to cache, but not when there's no data
        if data[:_meta][:hostvars].empty?
          exit 1
        else
          write_inventory_cache data
          read_inventory_cache
        end
      else
        # If cached data is sufficent, we don't need to do anything special
        read_inventory_cache
      end
    end
    # rubocop:enable Metrics/AbcSize

    private

    def write_inventory_cache(data)
      # Should we create @cache_file directory structure right here if it does
      # not already exist?
      File.open(@cache_file, 'w') do |file|
        # Write the data as JSON string to the cache file
        file.write JSON.pretty_generate(data)
        file.close
      end
    end

    # rubocop:disable Metrics/AbcSize
    def compile_meta
      # Create a container for metadata/host variables that Ansible will use
      @meta            = {}
      @meta[:hostvars] = {}

      # Track active hypervisors for #host_aggregates which cannot get that
      # information itself
      @active_hypervisors = []
      @hypervisors.each do |hyp|
        # Get host_ip info about hypervisor since it's not passed in with
        # #hypervisors
        details = @compute.get_hypervisor_details hyp['id']
        @meta[:hostvars][details[:body]['hypervisor']['service']['host']] = {
          ansible_host: details[:body]['hypervisor']['host_ip'],
          ansible_user: @adioh_ssh_user
        }
        @active_hypervisors.push(
          details[:body]['hypervisor']['service']['host']
        )
      end
    end
    # rubocop:enable Metrics/AbcSize

    def read_inventory_cache
      # Read inventory from the cache if we can
      f    = File.open(@cache_file, 'r')
      data = f.read
      f.close
      puts data
    rescue
      # Return empty set if we cannot read the cache file
      # Omit any host group names and only supply _meta
      JSON.pretty_generate(
        _meta:  {
          hostvars: []
        }
      )
    end

    def authenticate!
      # TODO: Check token valididity here to prevent excess authentication
      # attempts
      @connection_params = {
        openstack_username:     ENV['OS_USERNAME'],
        openstack_api_key:      ENV['OS_PASSWORD'],
        openstack_project_name: ENV['OS_PROJECT_NAME'],
        # OS_AUTH_URL should be of the form:
        # http://keystone.example.com:5000/v3
        openstack_auth_url:     "#{ENV['OS_AUTH_URL']}/auth/tokens",
        openstack_domain_id:    'default'
      }
      @compute = Fog::Compute::OpenStack.new(
        @connection_params
      )
    end
  end
end
