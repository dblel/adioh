#!/usr/bin/env ruby

require_relative 'lib/adioh'

if ARGV.first == '--list'
  @i = ADIOH::OpenStackInventory.new(
    adioh_cache_file: ENV['ADIOH_CACHE_FILE'],
    adioh_ssh_user:   ENV['ADIOH_SSH_USER']
  )
  @i.emit_inventory
elsif ARGV.first == '--refresh'
  @i = ADIOH::OpenStackInventory.new(
    adioh_cache_file: ENV['ADIOH_CACHE_FILE'],
    adioh_ssh_user:   ENV['ADIOH_SSH_USER']
  )
  @i.emit_inventory(true)
elsif ARGV.first == '--mock'
  Fog.mock!
  @i = ADIOH::OpenStackInventory.new(
    adioh_cache_file: '/tmp/.adioh_inventory_mock.json',
    adioh_ssh_user:   'mock'
  )
  @i.emit_inventory(true)
else
  exit 1
end
